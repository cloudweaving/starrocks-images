# starrocks-images

#### 介绍
star-manager部署时使用的starrocks镜像，与官方在k8s下面的镜像基本相同。

#### 软件架构
docker打包脚本

#### 使用说明

1.  先打包base镜像，fe和be镜像依赖于base镜像
2.  打包fe和be

打包示例脚本如下：

```
docker build -t datafabric-starrocks-base:2.5.13 .
docker build -t datafabric-starrocks-fe:2.5.13 .
docker build -t datafabric-starrocks-fe:2.5.13 .
```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


