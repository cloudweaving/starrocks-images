docker run -itd --name=starrocks-be --net=host --restart=unless-stopped \
-e FE_LEADER_IP=127.0.0.1 \
-e LOG_CONSOLE=1 \
-v /home/sr/starrocks/be/bin:/home/sr/starrocks/be/bin \
-v /home/sr/starrocks/be/conf:/home/sr/starrocks/be/conf \
-v /home/sr/starrocks/be/storage:/home/sr/starrocks/be/storage \
-v /home/sr/starrocks/be/log:/home/sr/starrocks/be/log \
 datafabric-starrocks-be:2.5.13
 