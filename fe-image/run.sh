docker run -itd --name=starrocks-fe --net=host --restart=unless-stopped \
-e FE_LEADER_IP=127.0.0.1 \
-e LOG_CONSOLE=1 \
-v /home/sr/starrocks/fe/bin:/home/sr/starrocks/fe/bin \
-v /home/sr/starrocks/fe/conf:/home/sr/starrocks/fe/conf \
-v /home/sr/starrocks/fe/meta:/home/sr/fe/meta \
-v /home/sr/starrocks/fe/log:/home/sr/starrocks/fe/log \
 datafabric-starrocks-fe:2.5.13